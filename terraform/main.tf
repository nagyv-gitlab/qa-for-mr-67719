terraform {
  required_version = ">= 1.0.0"
  backend "http" {
  }
}

resource "random_pet" "name" {
  keepers = {
    # Generate a new pet name each time we switch to a new AMI id
    now = timestamp()
  }
  length = 2
  separator = " "
}

output "name" {
    value = random_pet.name.id
}
